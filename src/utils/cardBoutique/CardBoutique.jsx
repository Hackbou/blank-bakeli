import { ShoppingCart, Visibility } from '@mui/icons-material';
import React from 'react';
import './cardBoutique.scss';

export default function CardBoutique() {
    return (
        <div className="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="product-grid">
                        <div class="product-image">
                            <a href="#" class="image">
                                <img class="img-1" src="assets/vetements/vetement (1).jpg" />
                            </a>
                            <ul class="product-links">
                                <li><a href="#"><i class="fas fa-shopping-cart"></i></a></li>
                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-eye"></i></a></li>
                                <li><a href="#"><i class="fa fa-random"></i></a></li>
                            </ul>
                        </div>
                        <div class="product-content">
                            <div class="price">$77.99</div>
                            <h3 class="title"><a href="#">Women's T-Shirt</a></h3>
                            <ul class="rating">
                                <li class="fas fa-star"></li>
                                <li class="fas fa-star"></li>
                                <li class="fas fa-star"></li>
                                <li class="far fa-star"></li>
                                <li class="far fa-star"></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="product-grid">
                        <div class="product-image">
                            <a href="#" class="image">
                                <img class="img-1" src="assets/vetements/vetement (2).jpg" />
                            </a>
                            <ul class="product-links">
                                <li><a href="#"><i class="fas fa-shopping-cart"></i></a></li>
                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-eye"></i></a></li>
                                <li><a href="#"><i class="fa fa-random"></i></a></li>
                            </ul>
                        </div>
                        <div class="product-content">
                            <div class="price">$61.99 <span>$79.11</span></div>
                            <h3 class="title"><a href="#">Women's Top</a></h3>
                            <ul class="rating">
                                <li class="fas fa-star"></li>
                                <li class="fas fa-star"></li>
                                <li class="fas fa-star"></li>
                                <li class="fas fa-star"></li>
                                <li class="far fa-star"></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="product-grid">
                        <div class="product-image">
                            <a href="#" class="image">
                                <img class="img-1" src="assets/vetements/vetement (1).jpg" />
                            </a>
                            <ul class="product-links">
                                <li><a href="#"><i class="fas fa-shopping-cart"></i></a></li>
                                <li><a href="#"><i class="fa fa-eye"></i></a></li>
                                <li><a href="#"><i class="fa fa-random"></i></a></li>
                            </ul>
                        </div>
                        <div class="product-content">
                            <div class="price">$77.99</div>
                            <h3 class="title"><a href="#">Women's T-Shirt</a></h3>
                            <ul class="rating">
                                <li class="fas fa-star"></li>
                                <li class="fas fa-star"></li>
                                <li class="fas fa-star"></li>
                                <li class="far fa-star"></li>
                                <li class="far fa-star"></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="product-grid">
                        <div class="product-image">
                            <a href="#" class="image">
                                <img class="img-1" src="assets/vetements/vetement (2).jpg" />
                            </a>
                            <ul class="product-links">
                            </ul>
                        </div>
                        <div class="product-content">
                            <div class="price">$61.99 <span>$79.11</span></div>
                            <h3 class="title"><a href="#">Women's Top</a></h3>
                            <ul class="rating">
                                <li class="fas fa-star"></li>
                                <li class="fas fa-star"></li>
                                <li class="fas fa-star"></li>
                                <li class="fas fa-star"></li>
                                <li class="far fa-star"></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
