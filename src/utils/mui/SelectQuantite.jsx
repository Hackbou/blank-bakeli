import { Add, Remove } from '@mui/icons-material';
import { IconButton } from '@mui/material';
import React from 'react';
import { useState } from 'react';
import './selects.scss';

export default function SelectQuantite(props) {

    const [quantite, selectQuantite] = useState(0);



    return (
        <div className="selectQuantite my-5">
            <p>{props.Title}</p>
            <IconButton onClick={() => quantite > 0 && (selectQuantite(quantite - 1))} aria-label="remove" size="small">
                <Remove />
            </IconButton>
            <span className="mx-1">{quantite}</span>
            <IconButton onClick={() => quantite >= 0 && selectQuantite(quantite + 1)} aria-label="add" size="small">
                <Add />
            </IconButton>
        </div>
    )
}
