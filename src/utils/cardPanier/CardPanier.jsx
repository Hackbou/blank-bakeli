import React from 'react';
import { Col, Row } from 'react-bootstrap';
import SelectQuantite from '../mui/SelectQuantite';
import './cardPanier.scss';

export default function CardPanier() {
    return (
        <Row>
            <Col className="col-12">
                <div className="cardPanier">
                    <div className="cardPanier-img">
                        <span>2000 fcfa</span>
                        <img src="assets/products/product_1.jpg" alt="" />
                    </div>
                    <div className="cardPanier-body">
                        <h3 className="h3">Title of card</h3>
                        <p className="select-quantite">
                            Total : <span>2500 fcfa</span>
                        </p>
                    </div>
                    <div className="quantite-cardPanier">
                        <SelectQuantite Title="" />
                    </div>
                </div>
            </Col>
        </Row>
    )
}
