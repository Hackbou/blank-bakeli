import React from 'react';
import Favorite from '../../components/favorite/Favorite';
import Header2 from '../../components/header/Header2';
import Section from '../../components/section/Section';
import './home.scss';

const Home = () => {
    return (
        <div className="home">
            <Header2 />
            <Section />
            <Favorite />
        </div>
    );
}

export default Home;
