import { Container } from '@mui/material';
import React from 'react';
import { Col, Row } from 'react-bootstrap';
import CardPanier from '../../utils/cardPanier/CardPanier';
import './panier.scss';

const Panier = () => {
    return (
        <Container className="my-5">
            <Row>
                <Col className='col-md-8 col-12'>
                    <CardPanier />
                    <CardPanier />
                    <CardPanier />
                    <CardPanier />
                </Col>
                <Col className='col-md-4 col-12'>
                    Je suis dans ma page panier veiller m'aider
                </Col>
            </Row>
        </Container>
    );
}

export default Panier;
