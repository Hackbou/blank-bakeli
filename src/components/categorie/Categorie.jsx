import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import CardCategorie from '../../utils/cardCategorie/CardCategorie';

const Categorie = () => {
    return (
        <Container className="py-4 mb-5">
            <Row className="text-center py-4">
                <Col>
                    <h3 style={{ textTransform: 'uppercase' }}>Categories</h3>
                </Col>
            </Row>
            <Row>
                <CardCategorie
                    Image="https://images.pexels.com/photos/3727464/pexels-photo-3727464.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                    Title="Title card"
                    Description="I am a description card"
                    Text="Simplement du text card"
                />

                <CardCategorie
                    Image="https://images.pexels.com/photos/3965548/pexels-photo-3965548.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                    Title="Title card"
                    Description="I am a description card"
                    Text="Simplement du text card"
                />

                <CardCategorie
                    Image="https://images.pexels.com/photos/267320/pexels-photo-267320.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                    Title="Title card"
                    Description="I am a description card"
                    Text="Simplement du text card"
                />

                <CardCategorie
                    Image="https://images.pexels.com/photos/318236/pexels-photo-318236.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                    Title="Title card"
                    Description="I am a description card"
                    Text="Simplement du text card"
                />
            </Row>
        </Container>
    );
}

export default Categorie;
