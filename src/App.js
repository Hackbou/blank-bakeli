import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';
import Footer from './components/footer/Footer';
// import Header from './components/header/Header';
import Header from './components/header/Header';
import Boutique from './pages/boutique/Boutique';
import Home from './pages/home/Home';
import ScrollToTop from "react-scroll-to-top";

//Navigation Component
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import Singleproduct from './pages/SingleProduct/SingleProduct';
import Panier from './pages/panier/Panier';


function App() {
  return (
    <BrowserRouter>
      <Header />

      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="boutique" element={<Boutique />} />
        <Route path="singleProduct" element={<Singleproduct />} />
        <Route path="/panier" element={<Panier />} />
      </Routes>

      <Footer />
      <ScrollToTop smooth />
    </BrowserRouter>
  );
}

export default App;
